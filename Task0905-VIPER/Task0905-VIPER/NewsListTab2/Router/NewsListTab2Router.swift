//
//  NewsListTab2Router.swift
//  Task0905-VIPER
//
//  Created by CPU12131 on 5/16/18.
//  Copyright © 2018 CPU12131. All rights reserved.
//

import Foundation
import UIKit

class NewsListTab2Router: NewsListTab2RouterProtocol{

    static func getTab2Module(from rootTab:UITabBarController) -> UIViewController {
        
        if let navigationController2 = rootTab.childViewControllers[1] as? UINavigationController {
            if let secondTabView = navigationController2.childViewControllers.first as? NewsListTab2View {

                let presenter = NewsListTab2Presenter()
                let interactor = NewsListTab2Interactor()
                let router = NewsListTab2Router()
                let localDataManager = NewsListLocalDataManager()

                secondTabView.presenter = presenter
                presenter.interactor = interactor
                presenter.router = router
                presenter.view = secondTabView
                interactor.presenter = presenter
                interactor.localDataManager = localDataManager

                return navigationController2
            }
        }
        return UIViewController()
    }
    
    func presentNewsDetailScreen(from view: NewsListTab2ViewProtocol, forNews news: NewsModel) {
        let newsDetailView = NewsDetailRouter.getNewsDetailViewModule(forNews: news)
        
        if let sourceView = view as? UIViewController{
            sourceView.navigationController?.pushViewController(newsDetailView, animated: true)
        }
    }
    
    
}
