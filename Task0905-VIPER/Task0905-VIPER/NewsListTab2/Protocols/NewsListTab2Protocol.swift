//
//  NewsListTab2Protocol.swift
//  Task0905-VIPER
//
//  Created by CPU12131 on 5/16/18.
//  Copyright © 2018 CPU12131. All rights reserved.
//

import Foundation

import UIKit

protocol NewsListTab2ViewProtocol: class {
    var presenter: NewsListTab2PresenterProtocol? {get set}
    
    // PRESENTER --> VIEW
    func showAllNews(with allNews:[NewsModel])
}

protocol NewsListTab2PresenterProtocol: class {
    var view: NewsListTab2ViewProtocol? {get set}
    var router: NewsListTab2RouterProtocol? {get set}
    var interactor: NewsListTab2InteractorInputProtocol? {get set}
    
    // VIEW --> PRESENTER
    func viewDidLoad()
    func viewNewsDetail(forNews news:NewsModel)
}

protocol NewsListTab2RouterProtocol: class {
    static func getTab2Module(from rootTab:UITabBarController) -> UIViewController
    
    // PRESENTER --> ROUTER
    func presentNewsDetailScreen(from view: NewsListTab2ViewProtocol, forNews news: NewsModel)
}

protocol NewsListTab2InteractorInputProtocol: class {
    var presenter: NewsListTab2InteractorOutputProtocol? {get set}
    var localDataManager: NewsListLocalDataManagerInputProtocol? {get set}
    
    // PRESENTER --> INTERACTOR
    func getNewsList()
}

protocol NewsListTab2InteractorOutputProtocol: class {
    // INTERACTOR --> PRESENTER
    func didReceivedNewsList(_ newsList: [NewsModel])
}
