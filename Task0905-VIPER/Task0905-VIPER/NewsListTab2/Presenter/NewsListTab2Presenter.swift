//
//  NewsListTab2Presenter.swift
//  Task0905-VIPER
//
//  Created by CPU12131 on 5/16/18.
//  Copyright © 2018 CPU12131. All rights reserved.
//

import Foundation

class NewsListTab2Presenter: NewsListTab2PresenterProtocol{
    
    var view: NewsListTab2ViewProtocol?
    var router: NewsListTab2RouterProtocol?
    var interactor: NewsListTab2InteractorInputProtocol?
    
    func viewDidLoad() {
        interactor?.getNewsList()
    }
    
    func viewNewsDetail(forNews news: NewsModel) {
        router?.presentNewsDetailScreen(from: view!, forNews: news)
    }
}

extension NewsListTab2Presenter: NewsListTab2InteractorOutputProtocol{
    
    func didReceivedNewsList(_ newsList: [NewsModel]) {
        view?.showAllNews(with: newsList)
    }
}
