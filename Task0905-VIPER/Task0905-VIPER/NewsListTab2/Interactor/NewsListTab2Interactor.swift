//
//  NewsListTab2Interactor.swift
//  Task0905-VIPER
//
//  Created by CPU12131 on 5/16/18.
//  Copyright © 2018 CPU12131. All rights reserved.
//

import Foundation

class NewsListTab2Interactor: NewsListTab2InteractorInputProtocol{
    
    var presenter: NewsListTab2InteractorOutputProtocol?
    var localDataManager: NewsListLocalDataManagerInputProtocol?
    
    func getNewsList() {
        guard let newsList = localDataManager?.getNewsList(from: "data2") else {
            print("GET DATA FROM FILE FAILED!")
            return
        }
        if newsList.count > 0 {
            presenter?.didReceivedNewsList(newsList)
        }
    }
}
