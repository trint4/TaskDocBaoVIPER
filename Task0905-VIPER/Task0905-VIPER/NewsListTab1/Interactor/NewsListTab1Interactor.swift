//
//  NewsListInteractorInput.swift
//  Task0905-VIPER
//
//  Created by CPU12131 on 5/11/18.
//  Copyright © 2018 CPU12131. All rights reserved.
//

import Foundation

class NewsListTab1Interactor: NewsListTab1InteractorInputProtocol{
    
    var presenter: NewsListTab1InteractorOutputProtocol?
    var localDataManager: NewsListLocalDataManagerInputProtocol?
    
    func getNewsList() {
        guard let newsList = localDataManager?.getNewsList(from: "data1") else {
            print("GET DATA FROM FILE FAILED!")
            return
        }
        if newsList.count > 0 {
            presenter?.didReceivedNewsList(newsList)
        }
    }
}
