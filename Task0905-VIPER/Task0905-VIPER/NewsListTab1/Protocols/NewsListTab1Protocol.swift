//
//  NewsListProtocol.swift
//  Task0905-VIPER
//
//  Created by CPU12131 on 5/11/18.
//  Copyright © 2018 CPU12131. All rights reserved.
//

import Foundation
import UIKit

protocol NewsListTab1ViewProtocol: class {
    var presenter: NewsListTab1PresenterProtocol? {get set}
    
    // PRESENTER --> VIEW
    func showAllNews(with allNews:[NewsModel])
}

protocol NewsListTab1PresenterProtocol: class {
    var view: NewsListTab1ViewProtocol? {get set}
    var router: NewsListTab1RouterProtocol? {get set}
    var interactor: NewsListTab1InteractorInputProtocol? {get set}
    
    // VIEW --> PRESENTER
    func viewDidLoad()
    func viewNewsDetail(forNews news:NewsModel)
}

protocol NewsListTab1RouterProtocol: class {
    static func getTab1Module(from rootTab: UITabBarController) -> UIViewController
    
    // PRESENTER --> ROUTER
    func presentNewsDetailScreen(from view: NewsListTab1ViewProtocol, forNews news: NewsModel)
}

protocol NewsListTab1InteractorInputProtocol: class {
    var presenter: NewsListTab1InteractorOutputProtocol? {get set}
    var localDataManager: NewsListLocalDataManagerInputProtocol? {get set}
    
    // PRESENTER --> INTERACTOR
    func getNewsList()
}

protocol NewsListTab1InteractorOutputProtocol: class {
    // INTERACTOR --> PRESENTER
    func didReceivedNewsList(_ newsList: [NewsModel])
}

protocol NewsListLocalDataManagerInputProtocol: class {
    // INTERACTOR -> LOCALDATAMANAGER
    func getNewsList(from fileName:String) -> [NewsModel]
}
