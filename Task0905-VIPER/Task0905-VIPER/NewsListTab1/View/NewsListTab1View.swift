//
//  News1View.swift
//  Task0905-VIPER
//
//  Created by CPU12131 on 5/9/18.
//  Copyright © 2018 CPU12131. All rights reserved.
//

import UIKit

class NewsListTab1View: UIViewController {

    var newsList = [NewsModel]()
    var presenter: NewsListTab1PresenterProtocol?
    lazy var refreshControl = UIRefreshControl()
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshControl.addTarget(self, action: #selector(loadMore(_:)), for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.blue
        tableView.addSubview(refreshControl)
        presenter?.viewDidLoad()
    }
    
    @objc func loadMore(_ sender:AnyObject){
        presenter?.viewDidLoad()
        refreshControl.endRefreshing()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = false
    }
}

extension NewsListTab1View: NewsListTab1ViewProtocol{
    
    func showAllNews(with allNews: [NewsModel]) {
        newsList = allNews
        tableView.reloadData()
    }
}

extension NewsListTab1View: UITableViewDataSource, UITableViewDelegate{

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "NewsTableViewCell", for: indexPath) as! NewsTableViewCell
        cell.set(news: newsList[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.viewNewsDetail(forNews: newsList[indexPath.row])
    }
}

