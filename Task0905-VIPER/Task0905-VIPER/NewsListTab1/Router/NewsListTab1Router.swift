//
//  NewsListRouter.swift
//  Task0905-VIPER
//
//  Created by CPU12131 on 5/11/18.
//  Copyright © 2018 CPU12131. All rights reserved.
//

import Foundation
import UIKit

class NewsListTab1Router: NewsListTab1RouterProtocol{

    static func getTab1Module(from rootTab: UITabBarController) -> UIViewController {
      
            if let navigationController1 = rootTab.childViewControllers.first as? UINavigationController {
                if let firstTabView = navigationController1.childViewControllers.first as? NewsListTab1View {
                    let presenter = NewsListTab1Presenter()
                    let interactor = NewsListTab1Interactor()
                    let router = NewsListTab1Router()
                    let localDataManager = NewsListLocalDataManager()
                    
                    firstTabView.presenter = presenter
                    presenter.interactor = interactor
                    presenter.router = router
                    presenter.view = firstTabView
                    interactor.presenter = presenter
                    interactor.localDataManager = localDataManager
                    
                    return navigationController1
                }
            }
        
        return UIViewController()
    }
    
    func presentNewsDetailScreen(from view: NewsListTab1ViewProtocol, forNews news: NewsModel) {
        let newsDetailView = NewsDetailRouter.getNewsDetailViewModule(forNews: news)
        
        if let sourceView = view as? UIViewController{
            sourceView.navigationController?.pushViewController(newsDetailView, animated: true)
        }
    }
}
