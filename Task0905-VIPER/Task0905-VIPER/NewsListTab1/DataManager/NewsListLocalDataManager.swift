//
//  NewsListLocalDataManagerInput.swift
//  Task0905-VIPER
//
//  Created by CPU12131 on 5/11/18.
//  Copyright © 2018 CPU12131. All rights reserved.
//

import Foundation

class NewsListLocalDataManager: NewsListLocalDataManagerInputProtocol{
    
    var newsAmountToGet = 0
    
    func getNewsList(from fileName:String) -> [NewsModel]{
        var newsList = [NewsModel]()
        guard let url = Bundle.main.url(forResource: fileName, withExtension: "json") else {return newsList}
        guard let data = try? Data(contentsOf: url) else {return newsList}
        
        do {
            let object = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
            guard let dictionary = object as? [String:AnyObject] else {return newsList}
            guard let allNews = dictionary["news"]! as? NSMutableArray else {return newsList}
            var i = 0
            newsAmountToGet += 4
            for news in allNews{
                guard let news = news as? [String:AnyObject] else { continue }
                let eachNews = NewsModel(id: news["id"] as! Int,
                                         header: news["header"] as! String,
                                         content: news["content"] as! String,
                                         smallImage: news["smallImage"] as! String,
                                         largeImage: news["largeImage"] as! String,
                                         source: news["source"] as! String,
                                         views: news["views"] as! Int)
                newsList.append(eachNews)
                i += 1
                if(i%newsAmountToGet == 0){
                    break
                }
            }
        } catch {
            print("--------ERROR--------")
        }
        if newsAmountToGet > newsList.count{
            newsAmountToGet = newsList.count
        }
        return newsList
    }
}
