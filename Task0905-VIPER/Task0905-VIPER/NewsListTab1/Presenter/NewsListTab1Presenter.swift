//
//  NewsListPresenter.swift
//  Task0905-VIPER
//
//  Created by CPU12131 on 5/11/18.
//  Copyright © 2018 CPU12131. All rights reserved.
//

import Foundation

class NewsListTab1Presenter: NewsListTab1PresenterProtocol{
    
    var view: NewsListTab1ViewProtocol?
    var router: NewsListTab1RouterProtocol?
    var interactor: NewsListTab1InteractorInputProtocol?
    
    func viewDidLoad() {
        interactor?.getNewsList()
    }
    
    func viewNewsDetail(forNews news: NewsModel) {
        router?.presentNewsDetailScreen(from: view!, forNews: news)
    }
}

extension NewsListTab1Presenter: NewsListTab1InteractorOutputProtocol{
    
    func didReceivedNewsList(_ newsList: [NewsModel]) {
        view?.showAllNews(with: newsList)
    }
}
